/*
Dealer Name: i.e.SmartSytems
System Name: IESS Codec v1.0
System Number:
Programmer: Matthew Laletas
Comments: IESS Codec v1.0
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
#DEFINE_CONSTANT NUM_INPUT 8
#HELP "IESS - Codec v0.1"
#HELP ""
#HELP "INPUTS:"
#HELP "StatusCall_Connected_FB: Feed this from the Codec module"
#HELP "StatusCall_Disconnected_FB: Feed this from the Codec module"
#HELP "Dial_Send_End: The toggle button from the TP, will determine whether to use DTMF or add to the DialString internally"
#HELP "Keypad_X: From the TP, will determine whether to use DTMF or add to the DialString internally"
#HELP "ContentStop: From the TP will pulse the Content_Stop and bring ContentSource to 0"
#HELP "ContentSources[X]: From the content source on the TP, will pulse the Content_Start and bring ContentSource to that same value"
#HELP "ContentSourceAnalog: This can be brought in from the NoTouch module, will pulse the Content_Start and bring ContentSource to that same value"
#HELP ""
#HELP "OUTPUTS:"
#HELP "Dial_Send: Tie this into Dial command of the Codec module, the DialString will have the number that was entered"
#HELP "Dial_End: Tie this into the End command of the Codec module"
#HELP "DTMF_X: Tie this into the DTMF commands of the Codec module, this will be used depending on what state the StatusCall FB is"
#HELP "Content_Start: Will pulse if the Digital or Analog content is change (positive)"
#HELP "Content_Start: Will pulse if the Analog is 0 or the ContentStop is brought high"
#HELP "ContentSource: Shows which content source is being used"
#HELP "DialString: Tie this into the Dial string of the Codec module"
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT StatusCall_Connected_FB, StatusCall_Disconnected_FB, _SKIP_;
DIGITAL_INPUT Dial_Send_End, _SKIP_, SelfViewToggle, AutoAnswerToggle, _SKIP_ ;
DIGITAL_INPUT Keypad_0, Keypad_1, Keypad_2, Keypad_3, Keypad_4, Keypad_5, Keypad_6, Keypad_7, Keypad_8, Keypad_9, Keypad_Star, Keypad_Pound, Keypad_Dot, Keypad_BackSpace_Clear, _SKIP_;
DIGITAL_INPUT ContentStop, ContentSources[NUM_INPUT];
ANALOG_INPUT ContentSourceAnalog, _SKIP_;
DIGITAL_OUTPUT Dial_Send, Dial_End, _SKIP_;
DIGITAL_OUTPUT SelfView_On, SelfView_Off, AutoAnswer_On, AutoAnswer_Off, _SKIP_;
DIGITAL_OUTPUT DTMF_0, DTMF_1, DTMF_2, DTMF_3, DTMF_4, DTMF_5, DTMF_6, DTMF_7, DTMF_8, DTMF_9, DTMF_Star, DTMF_Pound, DTMF_Dot, _SKIP_;
DIGITAL_OUTPUT Content_Start, Content_Stop, _SKIP_;
ANALOG_OUTPUT ContentSource, _SKIP_;
STRING_OUTPUT DialString;
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER GLBL_StatusCall, GLBL_SelfView, GLBL_AutoAnswer;
STRING GLBL_DialString[50];
/*******************************************************************************************
  Functions
*******************************************************************************************/
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
PUSH Dial_Send_End
{
	IF( GLBL_StatusCall )
		PULSE( 20, Dial_End );
	ELSE
		PULSE( 20, Dial_Send );
}
PUSH SelfViewToggle
{
	GLBL_SelfView = !GLBL_SelfView;
	IF( GLBL_SelfView )
	{
		SelfView_Off = 0;
		SelfView_On = 1;
	}
	ELSE
	{
		SelfView_On = 0;
		SelfView_Off = 1;
	}
}
PUSH AutoAnswerToggle
{
	GLBL_AutoAnswer = !GLBL_AutoAnswer;
	IF( GLBL_AutoAnswer )
	{
		AutoAnswer_Off = 0;
		AutoAnswer_On = 1;
	}
	ELSE
	{
		AutoAnswer_On = 0;
		AutoAnswer_Off = 1;
	}
}		
PUSH ContentSources
{
	INTEGER lvSource;
	lvSource = GETLASTMODIFIEDARRAYINDEX();
	ContentSource = lvSource;
	PULSE( 20, Content_Start );			
}
PUSH ContentStop
{
	ContentSource = 0;
	Content_Start = 0;
	PULSE( 20, Content_Stop );
}
CHANGE ContentSourceAnalog
{
	ContentSource = ContentSourceAnalog;
	IF( ContentSourceAnalog )
		PULSE( 20, Content_Start );
	ELSE
	{
		Content_Start = 0;
		PULSE( 20, Content_Stop );
	}
}		
CHANGE StatusCall_Connected_FB
{
	IF( StatusCall_Connected_FB )
		GLBL_StatusCall = 1;
	ELSE
		GLBL_StatusCall = 0;
	GLBL_DialString = "";
	DialString = GLBL_DialString;		
}
CHANGE StatusCall_Disconnected_FB
{
	IF( StatusCall_Disconnected_FB )
		GLBL_StatusCall = 0;
	ELSE
		GLBL_StatusCall = 1;
	GLBL_DialString = "";
	DialString = GLBL_DialString;
}
PUSH Keypad_0
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_0 );
	GLBL_DialString = GLBL_DialString + "0";
	DialString = GLBL_DialString;
}
PUSH Keypad_1
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_1 );
	GLBL_DialString = GLBL_DialString + "1";
	DialString = GLBL_DialString;
}
PUSH Keypad_2
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_2 );
	GLBL_DialString = GLBL_DialString + "2";
	DialString = GLBL_DialString;
}
PUSH Keypad_3
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_3 );
	GLBL_DialString = GLBL_DialString + "3";
	DialString = GLBL_DialString;
}
PUSH Keypad_4
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_4 );
	GLBL_DialString = GLBL_DialString + "4";
	DialString = GLBL_DialString;
}
PUSH Keypad_5
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_5 );
	GLBL_DialString = GLBL_DialString + "5";
	DialString = GLBL_DialString;
}
PUSH Keypad_6
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_6 );
	GLBL_DialString = GLBL_DialString + "6";
	DialString = GLBL_DialString;
}
PUSH Keypad_7
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_7 );
	GLBL_DialString = GLBL_DialString + "7";
	DialString = GLBL_DialString;
}
PUSH Keypad_8
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_8 );
	GLBL_DialString = GLBL_DialString + "8";
	DialString = GLBL_DialString;
}
PUSH Keypad_9
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_9 );
	GLBL_DialString = GLBL_DialString + "9";
	DialString = GLBL_DialString;
}
PUSH Keypad_Star
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_Star );
	GLBL_DialString = GLBL_DialString + "*";
	DialString = GLBL_DialString;
}
PUSH Keypad_Pound
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_Pound );
	GLBL_DialString = GLBL_DialString + "#";
	DialString = GLBL_DialString;
}
PUSH Keypad_Dot
{
	IF( GLBL_StatusCall )
		PULSE( 20, DTMF_Dot );
	GLBL_DialString = GLBL_DialString + ".";
	DialString = GLBL_DialString;
}
PUSH Keypad_BackSpace_Clear
{
	WAIT( 100, Clear )
	{
		IF( Keypad_BackSpace_Clear )
		{
			GLBL_DialString = "";
			DialString = GLBL_DialString;
		}
	}
}
RELEASE Keypad_BackSpace_Clear
{
	STRING lvString[50];
	IF( LEN( GLBL_DialString ) > 0 )
	{
		lvString = GLBL_DialString;
		lvString = REMOVEBYLENGTH( LEN( GLBL_DialString ) - 1, lvString );
		GLBL_DialString = lvString;
		DialString = GLBL_DialString;
	}
}
