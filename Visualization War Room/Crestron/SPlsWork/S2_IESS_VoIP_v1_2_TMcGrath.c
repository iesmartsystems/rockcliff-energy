#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_IESS_VoIP_v1_2_TMcGrath.h"

FUNCTION_MAIN( S2_IESS_VoIP_v1_2_TMcGrath );
EVENT_HANDLER( S2_IESS_VoIP_v1_2_TMcGrath );
DEFINE_ENTRYPOINT( S2_IESS_VoIP_v1_2_TMcGrath );














static void S2_IESS_VoIP_v1_2_TMcGrath__CALLSELECTION ( unsigned short __LVINDEX ) 
    { 
    /* Begin local function variable declarations */
    
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 87 );
    Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED = __LVINDEX; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 88 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_ANALOG_OUTPUT, __LVINDEX) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 89 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_STATE_ANALOG_OUTPUT, GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  )) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 90 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_TEXT  )    ,  __LVINDEX  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_STATE_TEXT_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 91 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NAME  )    ,  __LVINDEX  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_STATE_CID_NAME_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 92 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NUM  )    ,  __LVINDEX  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_STATE_CID_NUM_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 93 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  __LVINDEX  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__CALLSELECTION:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
static void S2_IESS_VoIP_v1_2_TMcGrath__CHANGECALLINACTIVESELECTION ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    unsigned short  __LVCALL; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 98 );
    __LVCALL = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 99 );
    __FN_FOREND_VAL__1 = 1; 
    __FN_FORINIT_VAL__1 = -( 1 ); 
    for( __LVCOUNTER = 6; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 101 );
        if ( !( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, __LVCOUNTER  ) )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 103 );
            __LVCALL = __LVCOUNTER; 
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 99 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 106 );
    if ( __LVCALL) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 107 );
        S2_IESS_VoIP_v1_2_TMcGrath__CALLSELECTION ( __LVCALL) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 109 );
        S2_IESS_VoIP_v1_2_TMcGrath__CALLSELECTION ( 1) ; 
        }
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__CHANGECALLINACTIVESELECTION:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
static void S2_IESS_VoIP_v1_2_TMcGrath__CHANGECALLACTIVESELECTION ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    unsigned short  __LVCALL; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 114 );
    __LVCALL = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 115 );
    __FN_FOREND_VAL__1 = 6; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 117 );
        if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, __LVCOUNTER  )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 119 );
            __LVCALL = __LVCOUNTER; 
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 115 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 122 );
    if ( __LVCALL) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 123 );
        S2_IESS_VoIP_v1_2_TMcGrath__CALLSELECTION ( __LVCALL) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 125 );
        S2_IESS_VoIP_v1_2_TMcGrath__CALLSELECTION ( 1) ; 
        }
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__CHANGECALLACTIVESELECTION:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00000 /*Dial_Send*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 132 );
    if ( !( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 133 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_VOIP_DIAL_SEND_DIG_OUTPUT ) ; 
        }
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_0:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00001 /*Dial_End*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 137 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 138 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_VOIP_DIAL_END_DIG_OUTPUT ) ; 
        }
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_1:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00002 /*Dial_Send_End*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 142 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 143 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_VOIP_DIAL_END_DIG_OUTPUT ) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 145 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_VOIP_DIAL_SEND_DIG_OUTPUT ) ; 
        }
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_2:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00003 /*VoIP_PopUP*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 149 );
    Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_POPUP = !( Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_POPUP ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 150 );
    SetDigital ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_VOIP_POPUP_ON_DIG_OUTPUT, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_POPUP) ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_3:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00004 /*StatusCall_Connected_FB*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 154 );
    if ( GetDigitalInput( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_STATUSCALL_CONNECTED_FB_DIG_INPUT )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 156 );
        SET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, 1 , 1) ; 
        } 
    
    else 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 160 );
        SET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, 1 , 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 161 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 162 );
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  1  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
        } 
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_4:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00005 /*StatusCall_Disconnected_FB*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 167 );
    if ( GetDigitalInput( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_STATUSCALL_DISCONNECTED_FB_DIG_INPUT )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 169 );
        SET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, 1 , 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 170 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 171 );
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  1  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
        } 
    
    else 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 175 );
        SET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, 1 , 1) ; 
        } 
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_5:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00006 /*Keypad_0*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "0" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "0" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 180 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 181 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_0_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 182 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 183 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_6:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00007 /*Keypad_1*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "1" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "1" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 187 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 188 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_1_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 189 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 190 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_7:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00008 /*Keypad_2*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "2" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "2" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 194 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 195 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_2_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 196 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 197 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_8:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00009 /*Keypad_3*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "3" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "3" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 201 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 202 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_3_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 203 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 204 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_9:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000A /*Keypad_4*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "4" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "4" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 208 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 209 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_4_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 210 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 211 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_10:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000B /*Keypad_5*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "5" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "5" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 215 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 216 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_5_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 217 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 218 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_11:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000C /*Keypad_6*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "6" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "6" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 222 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 223 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_6_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 224 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 225 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_12:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000D /*Keypad_7*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "7" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "7" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 229 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 230 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_7_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 231 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 232 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_13:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000E /*Keypad_8*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "8" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "8" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 236 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 237 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_8_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 238 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 239 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_14:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000F /*Keypad_9*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "9" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "9" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 243 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 244 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_9_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 245 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 246 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_15:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00010 /*Keypad_Star*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "*" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "*" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 250 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 251 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_STAR_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 252 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 253 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_16:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00011 /*Keypad_Pound*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "#" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "#" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 257 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 258 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_POUND_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 259 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 260 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_17:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00012 /*Keypad_Dot*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "." ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "." );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 264 );
    if ( GET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 265 );
        Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_DTMF_DOT_DIG_OUTPUT ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 266 );
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 267 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
    
    ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_18:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00013 /*Keypad_BackSpace_Clear*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 271 );
    CREATE_WAIT( S2_IESS_VoIP_v1_2_TMcGrath, 100, CLEAR );
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_19:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_WAITEVENT( S2_IESS_VoIP_v1_2_TMcGrath, CLEAR )
    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR___Wait1, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR___Wait1 );
    
    SAVE_GLOBAL_POINTERS ;
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR___Wait1 );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR___Wait1, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 273 );
    if ( GetDigitalInput( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_BACKSPACE_CLEAR_DIG_INPUT )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 275 );
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR___Wait1 )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR___Wait1 ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 276 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
        
        ; 
        } 
    
    

S2_IESS_VoIP_v1_2_TMcGrath_Exit__CLEAR:
    
    /* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    FREE_LOCAL_STRING_STRUCT( __FN_DST_STR___Wait1 );
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00014 /*Keypad_BackSpace_Clear*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __LVSTRING, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __LVSTRING );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__1, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__1 );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __LVSTRING );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVSTRING, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__1 );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__1, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 283 );
    if ( (Len( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )  ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ) > 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 285 );
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , LOCAL_STRING_STRUCT( __LVSTRING  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 286 );
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    , (Len( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )  ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  ) ) - 1), LOCAL_STRING_STRUCT( __LVSTRING  )  )  )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , LOCAL_STRING_STRUCT( __LVSTRING  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 287 );
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVSTRING  )   )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 288 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
        
        ; 
        } 
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_20:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __LVSTRING );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__1 );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00015 /*JoinCall*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 293 );
    S2_IESS_VoIP_v1_2_TMcGrath__CHANGECALLINACTIVESELECTION ( ) ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_21:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00016 /*Call_State*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_VoIP_v1_2_TMcGrath, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 298 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_VoIP_v1_2_TMcGrath ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 299 );
    if ( ((((GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  ) == 4) || (GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  ) == 6)) || (GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  ) == 7)) || ((GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  ) >= 13) && (GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  ) <= 18)))) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 301 );
        SET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, __LVINDEX , 1) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 302 );
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  __LVINDEX  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
        } 
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 304 );
        if ( (GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  ) == 8)) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 305 );
            Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_INCOMINGSELECTED = __LVINDEX; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 306 );
            if ( (GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  ) == 3)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 308 );
                SET_GLOBAL_INTARRAY_VALUE( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, __LVINDEX , 0) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 309 );
                FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  __LVINDEX  )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 310 );
                S2_IESS_VoIP_v1_2_TMcGrath__CHANGECALLACTIVESELECTION ( ) ; 
                } 
            
            }
        
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 312 );
    if ( (__LVINDEX == Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 314 );
        SetAnalog ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_STATE_ANALOG_OUTPUT, GetInOutArrayElement( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), &Globals->S2_IESS_VoIP_v1_2_TMcGrath.__CALL_STATE , __LVINDEX  )) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 315 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING  )    ,  Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED  )  )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DIALSTRING_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
        
        ; 
        } 
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_22:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00017 /*Call_State_Text*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 321 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_VoIP_v1_2_TMcGrath ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 322 );
    if ( (__LVINDEX == Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 323 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_TEXT  )    ,  __LVINDEX  )  )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_STATE_TEXT_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
        
        ; 
        }
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_23:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00018 /*Call_State_CID_Name*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 328 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_VoIP_v1_2_TMcGrath ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 329 );
    if ( (__LVINDEX == Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 330 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NAME  )    ,  __LVINDEX  )  )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_STATE_CID_NAME_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
        
        ; 
        }
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_24:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00019 /*Call_State_CID_Num*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 335 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_VoIP_v1_2_TMcGrath ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 336 );
    if ( (__LVINDEX == Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 337 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ),  GLOBAL_STRING_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NUM  )    ,  __LVINDEX  )  )  ; 
        SetSerial( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_STATE_CID_NUM_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_VoIP_v1_2_TMcGrath ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); }
        
        ; 
        }
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_25:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0001A /*Answer*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 341 );
    S2_IESS_VoIP_v1_2_TMcGrath__CALLSELECTION ( Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_INCOMINGSELECTED) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 342 );
    Pulse ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) , 20, __S2_IESS_VoIP_v1_2_TMcGrath_ANSWERCALL_DIG_OUTPUT ) ; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_26:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0001B /*Decline*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 346 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_ANALOG_OUTPUT, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_INCOMINGSELECTED) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 347 );
    SetDigital ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DECLINECALL_DIG_OUTPUT, 1) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 348 );
    CREATE_WAIT( S2_IESS_VoIP_v1_2_TMcGrath, 20, __SPLS_TMPVAR__WAITLABEL_0__ );
    
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__Event_27:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_WAITEVENT( S2_IESS_VoIP_v1_2_TMcGrath, __SPLS_TMPVAR__WAITLABEL_0__ )
    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 350 );
    SetDigital ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_DECLINECALL_DIG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 351 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_ANALOG_OUTPUT, Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED) ; 
    

S2_IESS_VoIP_v1_2_TMcGrath_Exit____SPLS_TMPVAR__WAITLABEL_0__:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }

    
    

/********************************************************************************
  Constructor
********************************************************************************/

/********************************************************************************
  Destructor
********************************************************************************/

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_VoIP_v1_2_TMcGrath_STATUSCALL_CONNECTED_FB_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                
            }
            else /*Release*/
            {
                
            }
            SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00004 /*StatusCall_Connected_FB*/, 0 );
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_STATUSCALL_DISCONNECTED_FB_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                
            }
            else /*Release*/
            {
                
            }
            SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00005 /*StatusCall_Disconnected_FB*/, 0 );
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_DIAL_SEND_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00000 /*Dial_Send*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_DIAL_END_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00001 /*Dial_End*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_DIAL_SEND_END_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00002 /*Dial_Send_End*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_VOIP_POPUP_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00003 /*VoIP_PopUP*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_0_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00006 /*Keypad_0*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_1_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00007 /*Keypad_1*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_2_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00008 /*Keypad_2*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_3_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00009 /*Keypad_3*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_4_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000A /*Keypad_4*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_5_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000B /*Keypad_5*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_6_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000C /*Keypad_6*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_7_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000D /*Keypad_7*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_8_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000E /*Keypad_8*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_9_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0000F /*Keypad_9*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_STAR_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00010 /*Keypad_Star*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_POUND_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00011 /*Keypad_Pound*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_DOT_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00012 /*Keypad_Dot*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_KEYPAD_BACKSPACE_CLEAR_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00013 /*Keypad_BackSpace_Clear*/, 0 );
                
            }
            else /*Release*/
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00014 /*Keypad_BackSpace_Clear*/, 0 );
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_ANSWER_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0001A /*Answer*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_DECLINE_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 0001B /*Decline*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_JOINCALL_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00015 /*JoinCall*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
                
            }
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00016 /*Call_State*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_VoIP_v1_2_TMcGrath ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_TEXT_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00017 /*Call_State_Text*/, 0 );
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_CID_NAME_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00018 /*Call_State_CID_Name*/, 0 );
            break;
            
        case __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_CID_NUM_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_VoIP_v1_2_TMcGrath, 00019 /*Call_State_CID_Num*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_VoIP_v1_2_TMcGrath ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_IESS_VoIP_v1_2_TMcGrath )
********************************************************************************/
EVENT_HANDLER( S2_IESS_VoIP_v1_2_TMcGrath )
    {
    SAVE_GLOBAL_POINTERS ;
    CHECK_INPUT_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_TEXT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NUM ) ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_IESS_VoIP_v1_2_TMcGrath )
********************************************************************************/
FUNCTION_MAIN( S2_IESS_VoIP_v1_2_TMcGrath )
{
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    SET_INSTANCE_POINTER ( S2_IESS_VoIP_v1_2_TMcGrath );
    
    
    /* End local function variable declarations */
    
    INITIALIZE_IO_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE, IO_TYPE_ANALOG_INPUT, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_ANALOG_INPUT, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_ARRAY_LENGTH );
    
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_TEXT, e_INPUT_TYPE_STRING, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_TEXT_ARRAY_NUM_ELEMS, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_TEXT_ARRAY_NUM_CHARS, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_TEXT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_TEXT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NAME, e_INPUT_TYPE_STRING, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_CID_NAME_ARRAY_NUM_ELEMS, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_CID_NAME_ARRAY_NUM_CHARS, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_CID_NAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NUM, e_INPUT_TYPE_STRING, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_CID_NUM_ARRAY_NUM_ELEMS, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_CID_NUM_ARRAY_NUM_CHARS, __S2_IESS_VoIP_v1_2_TMcGrath_CALL_STATE_CID_NUM_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __CALL_STATE_CID_NUM );
    INITIALIZE_GLOBAL_STRING_ARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_DIALSTRING, e_INPUT_TYPE_NONE, __S2_IESS_VoIP_v1_2_TMcGrath_GLBL_DIALSTRING_ARRAY_NUM_ELEMS, __S2_IESS_VoIP_v1_2_TMcGrath_GLBL_DIALSTRING_ARRAY_NUM_CHARS );
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_VoIP_v1_2_TMcGrath, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    INITIALIZE_GLOBAL_INTARRAY ( S2_IESS_VoIP_v1_2_TMcGrath, __GLBL_CALLACTIVE, 0, 6 );
    
    
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 356 );
    S2_IESS_VoIP_v1_2_TMcGrath__CALLSELECTION ( 1) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 357 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), __S2_IESS_VoIP_v1_2_TMcGrath_CALL_SELECTED_ANALOG_OUTPUT, 1) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 358 );
    Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_INCOMINGSELECTED = 1; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_VoIP_v1_2_TMcGrath ), 359 );
    Globals->S2_IESS_VoIP_v1_2_TMcGrath.__GLBL_CALLSELECTED = 1; 
    
    S2_IESS_VoIP_v1_2_TMcGrath_Exit__MAIN:/* Begin Free local function variables */
    /* End Free local function variables */
    
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
